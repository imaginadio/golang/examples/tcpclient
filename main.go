package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("usage: %s port_number\n", os.Args[0])
		return
	}

	port := ":" + os.Args[1]
	conn, err := net.Dial("tcp4", port)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	if err := serveConn(conn); err != nil {
		fmt.Println(err)
	}
}

func serveConn(conn net.Conn) error {
	stdReader := bufio.NewReader(os.Stdin)
	connReader := bufio.NewReader(conn)
	for {
		fmt.Print(">> ")
		strBytes, err := stdReader.ReadBytes('\n')
		if err != nil {
			return err
		}

		_, err = conn.Write(strBytes)
		if err != nil {
			return err
		}

		connResult, err := connReader.ReadString('\n')
		if err != nil {
			return err
		}

		fmt.Println("Response:", connResult)
	}
}
